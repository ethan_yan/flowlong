/* 
 * Copyright 2023-2025 Licensed under the AGPL License
 */
package com.aizuda.bpm.mybatisplus.mapper;

import com.aizuda.bpm.engine.entity.FlwProcess;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 流程定义 Mapper
 *
 * <p>
 * 尊重知识产权，不允许非法使用，后果自负
 * </p>
 *
 * @author hubin
 * @since 1.0
 */
public interface FlwProcessMapper extends BaseMapper<FlwProcess> {

}
